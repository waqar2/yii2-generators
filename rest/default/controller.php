<?php

use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator magisterapp\generators\rest\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use <?= ltrim($generator->baseControllerClass, '\\') ?>;

 /**
 * <?= $controllerClass ?> Clase encargada de presentar y manipular la información del modelo <?= $modelClass ?> para las solicitudes en el api
 *
 * @package app/modules
 * @subpackage api/controllers
 * @category Controllers
 *
 * @author <?= Yii::$app->params['developers'][$generator->developer] ?> <<?= $generator->developer ?>>
 * @copyright Copyright (c) <?= date('Y') . " {$generator->copyright} \n" ?>
 * @version 0.0.1
 * @since <?= $generator->since . "\n" ?>
 */
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClass) . "\n" ?>
{

    /**
     * @var string
     * Modelo para las operaciones CRUD
     */
    public $modelClass = '<?= $generator->modelClass ?>';

    /**
     * @var string
     * Modelo para las búsquedas
     */
    public $searchModel = '<?= $generator->searchModelClass ?>';

    /**
     * @var string
     * Llave primaria del modelo para la sincronización
     */
    public $primaryKey = '<?= $generator->getPrimaryKey() ?>';

    /**
     * Verifica los permisos del usuario actual (por defecto solo el admin puede realizarlas)
     *
     * @param string $action Identificador de la acción a ejecutar
     * @param \yii\base\Model $model Modelo a acceder
     * @param array $params Parámetros adicionales
     * @throws ForbiddenHttpException Sí el usuario no tiene permiso
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        if ($action === 'create' || $action === 'update' || $action === 'delete' || $action === 'restore') {
            if (\Yii::$app->user->id != 1)
                throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'No tiene permiso para realizar esta acción'));
        }
    }

}
