<?php


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator magisterapp\generators\rest\Generator */

echo $form->field($generator, 'pathModels');
echo $form->field($generator, 'models')->dropDownList(
    $generator->getModelFiles(),
    ['multiple' => true,]
);
echo $form->field($generator, 'pathModule');
echo $form->field($generator, 'module');
echo $form->field($generator, 'baseControllerClass');
echo $form->field($generator, 'developer')->dropDownList(Yii::$app->params['developers']);
echo $form->field($generator, 'copyright');
echo $form->field($generator, 'since');
