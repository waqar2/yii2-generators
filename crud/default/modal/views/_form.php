<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator magisterapp\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();

if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use <?= $generator->baseActiveForm ?>;
use <?= $generator->baseHelperUI ?>;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form <?= $generator->baseActiveForm ?> */
?>

<div class="<?php echo Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">
    <?php echo "<?php " ?>$form = ActiveForm::begin([
        'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form',
    ]); ?>
    <div class="row">
            <?php
            $index = 1;
            echo "\n";
            foreach ($generator->getColumnNames() as $attribute) {
                if (in_array($attribute, $safeAttributes) && $generator->isColumnVisible($attribute)) {
                    echo "\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n";
                    echo "\t\t\t\t<?= " . $generator->generateActiveField($attribute, $index) . " ?>\n\n";
                    echo "\t\t\t</div>\n";
                    $index++;
                }
            }
            echo "\n";
            ?>
    </div>

    <?php echo "<?php " ?>ActiveForm::end(); ?>
</div>
