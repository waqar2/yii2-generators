<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator magisterapp\generators\crud\Generator */

echo "<?php\n\n";
?>
use <?= $generator->baseTabs ?>;
use <?= $generator->baseActiveForm ?>;
use <?= $generator->baseHelperUI ?>;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form <?= $generator->baseActiveForm ?> */

$form = ActiveForm::begin([
    'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form',
]);
?>
<div class="<?php echo Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form <?= $generator->bsVersion > 3 ? 'tabs-container' : '' ?>">
    <?php echo "<?= " ?>Tabs::widget([
        'items' => [
            [
                'label'   => <?php echo $generator->generateString('Información') ?>,
                'content' => $this->render('1_informacion', ['form' => $form, 'model' => $model]),
                'active'  => true
            ],
        ],
    ]);
    ?>

    <div class="hr-line-dashed"></div>

    <div class="form-group row">
        <div class="col-sm-4 col-sm-offset-2">
            <?= "<?= " ?> UI::btnCancel(); ?>
            <?= "<?= " ?> UI::btnSend(); ?>
        </div>
    </div>

    <?php echo "<?php " ?>ActiveForm::end(); ?>
</div>
