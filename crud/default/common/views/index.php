<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator magisterapp\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$controllerId = Inflector::camel2id(StringHelper::basename($generator->modelClass));

echo "<?php\n";
?>

use <?= $generator->baseHelperHtml ?>;
use <?= $generator->baseHelperUI ?>;
use <?= $generator->baseHelperStrings ?>;
use <?= $generator->baseHelperUsuario ?>;
use <?php echo $generator->indexWidgetType === 'grid' ? "kartik\dynagrid\DynaGrid" : "yii\\widgets\\ListView" ?>;
use yii\<?= $generator->nameSapaceBs; ?>\Modal;
use kartik\grid\GridView;

/* @var $this yii\web\View */
<?php echo !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?php echo $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?php echo Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
        <?php if ($generator->indexWidgetType === 'grid') : ?>
<?php echo "<?php echo " ?>DynaGrid::widget([
            'columns' => [
                ['class'=>'kartik\grid\SerialColumn'],
                <?php
                $tableSchema = $generator->getTableSchema();
                $cont = 0;
                if ($tableSchema === false) {
                    foreach ($generator->getColumnNames() as $name) {
                        echo "\t\t\t\t[\n";
                        echo "\t\t\t\t\t'attribute' => '{$name}',\n";
                        echo "\t\t\t\t\t'visible' => " . ($generator->isColumnVisible($column) && $cont <= 6 ? 'true' : 'false') . ",\n";
                        echo "\t\t\t\t],\n";
                        $cont++;
                    }
                } else {
                    foreach ($tableSchema->columns as $column) {
                        $format = $generator->generateColumnFormat($column);
                        echo "\t\t\t\t[\n";
                        echo "\t\t\t\t\t'attribute' => '{$column->name}',\n";
                        if ($format !== 'text') {
                            echo "\t\t\t\t\t'format' => '{$format}',\n";
                        }
                        echo "\t\t\t\t\t'visible' => " . ($generator->isColumnVisible($column) && $cont <= 6 ? 'true' : 'false') . ",\n";

                        if (isset($generator->foreignFields[$column->name])) {
                            echo "\t\t\t\t\t'value' => function(\$model){\n\t\t\t\t\t\t\$name = \$model->{$generator->foreignFields[$column->name]['relation']}->getNameFromRelations();\n\t\t\t\t\t\treturn \$model->{$generator->foreignFields[$column->name]['relation']}->\$name;\n\t\t\t\t\t},\n";
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_SELECT2,\n";
                            echo "\t\t\t\t\t'filter' => {$generator->foreignFields[$column->name]['class']}::getData(),\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid', 'placeholder' => Strings::getTextAll()],\n";
                        } elseif ($column->name == $model::STATUS_COLUMN) {
                            echo "\t\t\t\t\t'value' => function(\$model){\n\t\t\t\t\t\treturn Strings::getCondition(\$model->" . $model::STATUS_COLUMN . ");\n\t\t\t\t\t},\n";
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_SELECT2,\n";
                            echo "\t\t\t\t\t'filter' => Strings::getCondition(),\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid', 'placeholder' => Strings::getTextAll()],\n";
                        } elseif ($column->name == $model::CREATED_AT_COLUMN) {
                            echo "\t\t\t\t\t'value' => function(\$model){\n\t\t\t\t\t\treturn \$model->creadoPor->username;\n\t\t\t\t\t},\n";
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_SELECT2,\n";
                            echo "\t\t\t\t\t'filter' => Usuario::getUsers(),\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid', 'placeholder' => Strings::getTextAll()],\n";
                        } elseif ($column->name == $model::UPDATED_AT_COLUMN) {
                            echo "\t\t\t\t\t'value' => function(\$model){\n\t\t\t\t\t\treturn \$model->modificadoPor->username;\n\t\t\t\t\t},\n";
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_SELECT2,\n";
                            echo "\t\t\t\t\t'filter' => Usuario::getUsers(),\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid', 'placeholder' => Strings::getTextAll()],\n";
                        } elseif ($format == 'datetime' || $format == "date" || $column->name == $model::CREATED_DATE_COLUMN || $column->name == $model::UPDATED_DATE_COLUMN) {
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_DATE,\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid'],\n";
                        }

                        echo "\t\t\t\t],\n";
                        $cont++;
                    }
                }
                ?>
                [
                    'class' => '<?= $generator->baseActionColumn ?>',
                    <?php
                    if ($generator->canUseModal()) {
                        echo "'isModal' => true,\n";
                        echo "\t\t\t\t\t'dynaModalId' => '{$controllerId}',\n";
                    }
                    ?>

                ],
            ],
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'panel' => [
                    'heading' => false,
                    'footer' => '{summary}',
                    'type' => 'default'
                ],
                'toolbar' => [
                    '{toggleData}',
                    [
                        'content' =>
                        <?php
                        if ($generator->canUseModal()) {
                            echo "\tUI::btnNew('', ['create'], ['onClick' => \"openModalGrid(this, '{$controllerId}', 'create'); return false;\"]) . ' '\n";
                        } else {
                            echo "\tUI::btnNew('') . ' '\n";
                        }
                        ?>
                            .UI::btnSearch('') . ' '
                            .UI::btnRefresh('')
                    ],
                    '{dynagrid}',
                    '{export}',
                ],
                'options' => ['id' => 'gridview-<?= $controllerId ?>'],
            ],
            'options' => ['id' => 'dynagrid-<?= $controllerId ?>'],
        ]) ?>
        <?php else : ?>
            <?php echo "<?php echo " ?>ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
                    return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                },
            ]) ?>
        <?php endif; ?>
</div>
<?php echo "<?php " ?>
Modal::begin([
    'id'      => 'search-modal',
<?php if($generator->bsVersion > 3):?>
    'title'  => Yii::t('app', 'Búsqueda Avanzada'),
<?php else:?>
    'header'  => '<h4 class="modal-title">'.Yii::t('app', 'Búsqueda Avanzada').'</h4>',
<?php endif;?>
    'size'    => Modal::SIZE_LARGE,
    'footer' => UI::btnCloseModal(Yii::t('app', 'Cancelar'), Html::ICON_REMOVE) . UI::btnSend(Yii::t('app', 'Buscar'),  ['form' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search-form']),
    'options' => [ 'style' => 'display: none;', 'tabindex' => '-1' ]
]);
echo $this->render('_search', ['model' => $searchModel]);
Modal::end();
<?php echo "?>" ?>

<?php if ($generator->canUseModal()) : ?>
<?php echo "<?php " ?>Modal::begin([
    'id'            =>'<?= $controllerId ?>-modal',
<?php if($generator->bsVersion > 3):?>
    'title'  => Yii::t('app', '<?php echo Inflector::singularize(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>'),
<?php else:?>
    'header'        => '<h4 class="modal-title">'.Yii::t('app', '<?php echo Inflector::singularize(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>').'</h4>',
<?php endif;?>
    'size'    => Modal::SIZE_LARGE,
    'footer' => UI::btnCloseModal(Yii::t('app', 'Cancelar'), Html::ICON_REMOVE) . UI::btnSend(null,  ['form' => '<?= $controllerId ?>-form']),
    'options'       => ['style' => 'display: none;', 'tabindex' => '-1'],
]);

Modal::end();
<?php echo "?>" ?>

<?php endif; ?>
