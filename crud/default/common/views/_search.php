<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator magisterapp\generators\crud\Generator */


echo "<?php\n";
?>

use <?= $generator->baseActiveForm ?>;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->searchModelClass, '\\') ?> */
/* @var $form <?= $generator->baseActiveForm ?> */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search">
    <?php echo "<?php " ?>$form = ActiveForm::begin([
        'id'     => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search-form',
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
            <?php
            $index = 1;
            $primaryKey = $model->getTableSchema()->primaryKey[0];
            echo "\n";
            foreach ($generator->getColumnNames() as $attribute) {
                if (($attribute == $primaryKey && $model->getTableSchema()->getColumn($primaryKey)->autoIncrement)) {
                    continue;
                }

                echo "\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n";
                echo "\t\t\t\t<?= " . $generator->generateActiveSearchField($attribute, $index) . " ?>\n\n";
                echo "\t\t\t</div>\n";

                $index++;
            }
            echo "\n";
            ?>
    </div>


    <?php echo "<?php " ?>ActiveForm::end(); ?>
</div>
