<?php

/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Éste es el modelo para la tabla "<?= $generator->generateTableName($tableName) ?>".
 * <?= $generator->getComment($tableName) . "\n" ?>
 *
 * @package app
 * @subpackage models/base
 * @category Models
 *
<?php foreach ($tableSchema->columns as $column) : ?>
 * @property <?= "{$column->phpType} \${$column->name} " . str_replace(["\n", "\r"], [" ", " "], $column->comment) . "\n" ?>
<?php endforeach; ?>
 *
<?php if (!empty($relations)) : ?>
<?php foreach ($relations as $name => $relation) : ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . " Datos relacionados con modelo \"{$relation[1]}\"\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 *
 * @author <?= Yii::$app->params['developers'][$generator->developer] ?> <<?= $generator->developer ?>>
 * @copyright Copyright (c) <?= date('Y') . " {$generator->copyright} \n" ?>
 * @version 0.0.1
 * @since <?= $generator->since . "\n" ?>
 */
class <?= $className ?> extends <?= '\\' . ltrim($generator->baseClass, '\\') . "\n" ?>
{

    /**
     * Nombre de la tabla que representa el modelo.
     *
     * @return string
     */
    public static function tableName()
    {
        return '<?= $generator->generateTableName($tableName) ?>';
    }
<?php if ($generator->db !== 'db') : ?>

    /**
     * Crear la conexión a la BD.
     *
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('<?= $generator->db ?>');
    }
<?php endif; ?>

    /**
     * Retorna las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [<?= "\n            " . implode(",\n            ", $rules) . ",\n        " ?>];
    }

    /**
     * Retorna los nombres de los campos.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
<?php foreach ($labels as $name => $label) : ?>
            <?= "'$name' => " . $generator->generateString($label) . ",\n" ?>
<?php endforeach; ?>
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
<?php foreach ($tableSchema->columns as $column) : ?>
            <?= "'{$column->name}' => " . $generator->generateString(str_replace(["\n", "\r"], [" ", " "], $column->comment)) . ",\n" ?>
<?php endforeach; ?>
        ];

        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

<?php foreach ($relations as $name => $relation) : ?>
    /**
     * Retorna los registros de la relación.
     *
     * @return \yii\db\ActiveQuery
     */
    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }
<?php endforeach; ?>
<?php if ($queryClassName) : ?>
<?php
$queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
echo "\n";
?>
    /**
     * Retorna una instacia de la clase actual.
     *
     * @return <?= $queryClassFullName ?> the active query used by this AR class.
     */
    public static function find()
    {
        return new <?= $queryClassFullName ?>(get_called_class());
    }
<?php endif; ?>
    /**
     * Método para retornar los Datos del Modelo

     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|<?= $className . "[]\n" ?>
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }

        $query = self::find()->where($attributes)->all();

        if ($list) {
            $query = ArrayHelper::map($query, '<?= is_array($tableSchema->primaryKey) ? $tableSchema->primaryKey[0] : $tableSchema->primaryKey; ?>', self::getNameFromRelations());
        }

        return $query;

    }
}
